##
## filter/Filter
##

identical(filter(starwars, species == "Human"),
          Filter(starwars, ~species == "Human"))

identical(filter(starwars, mass > 1000),
          Filter(starwars, ~ mass > 1000))

identical(filter(starwars, hair_color == "none" & eye_color == "black"),
          Filter(starwars, ~ hair_color == "none" & eye_color == "black"))
identical(filter(starwars, hair_color == "none" | eye_color == "black"),
          Filter(starwars, ~hair_color == "none" | eye_color == "black"))

identical(filter(starwars, hair_color == "none", eye_color == "black"),
          Filter(starwars, ~hair_color == "none", ~eye_color == "black"))

identical(starwars %>% filter(mass > mean(mass, na.rm = TRUE)),
          starwars %>% Filter(~mass > mean(mass, na.rm = TRUE)))

identical(
    starwars %>% group_by(gender) %>% filter(mass > mean(mass, na.rm = TRUE)),
    starwars %>% Group_by(~gender) %>% Filter(~mass > mean(mass, na.rm = TRUE)))

## This would _not_ work with proper use of formula environments.
## But with pipes you can use '.'; with intermedeate variables use those.
vars <- c("mass", "height")
cond <- c(80, 150)
identical(
    starwars %>%
    filter(
        .data[[vars[[1]]]] > cond[[1]],
        .data[[vars[[2]]]] > cond[[2]]
    ),
    starwars %>%
    Filter(
        ~.data[[vars[[1]]]] > cond[[1]],
        ~.data[[vars[[2]]]] > cond[[2]]
    ))

## This fails:
## Maybe it should: check in proper scoping context
mass <- 80
height <- 150
identical(filter(starwars, mass > !!mass, height > !!height),
          Filter(starwars, ~mass > !!mass, ~height > !!height))

## This works in this example; to be reliable the implementation would
## need to use the formula environment.
mymass <- 80
myheight <- 150
identical(filter(starwars, mass > mymass, height > myheight),
          Filter(starwars, ~ mass > mymass, ~ height > myheight))


##
## mutate/Mutate
##

mtcarsT <- as_tibble(mtcars)
identical(mutate(mtcarsT, cyl2 = cyl * 2, cyl4 = cyl2 * 2),
          Mutate(mtcarsT, cyl2 = ~ cyl * 2, cyl4 = ~ cyl2 * 2))

identical(mutate(mtcarsT, mpg = NULL, disp = disp * 0.0163871),
          Mutate(mtcarsT, mpg = NULL, disp = ~ disp * 0.0163871))

identical(mtcars %>%
          group_by(cyl) %>%
          mutate(rank = min_rank(desc(mpg))),
          mtcars %>%
          Group_by(~ cyl) %>%
          Mutate(rank = ~min_rank(desc(mpg))))

identical(mutate(mtcars, cyl = NULL), Mutate(mtcars, cyl = NULL))

identical(mutate(mtcars, displ_l = disp / 61.0237),
          Mutate(mtcars, displ_l = ~ disp / 61.0237))

identical(transmute(mtcars, displ_l = disp / 61.0237),
          Transmute(mtcars, displ_l = ~ disp / 61.0237))

identical(starwars %>% mutate(mass / mean(mass, na.rm = TRUE)) %>% pull(),
          starwars %>% Mutate(~ mass / mean(mass, na.rm = TRUE)) %>% pull())

identical(
    starwars %>%
    group_by(gender) %>%
    mutate(mass / mean(mass, na.rm = TRUE)) %>%
    pull(),
    starwars %>%
    Group_by(~ gender) %>%
    Mutate(~ mass / mean(mass, na.rm = TRUE)) %>%
    pull())

gdf <- mtcars %>% group_by(cyl)
identical(tryCatch(mutate(gdf, cyl = cyl * 100), error = identity),
          tryCatch(Mutate(gdf, cyl = ~ cyl * 100), error = identity))

## This would _not_ work with proper use of formula environments.
## But with pipes you can use '.'; with intermedeate variables use those.
vars <- c("mass", "height")
identical(mutate(starwars, prod = .data[[vars[[1]]]] * .data[[vars[[2]]]]),
          Mutate(starwars, prod = ~ .data[[vars[[1]]]] * .data[[vars[[2]]]]))

## This fails:
## Maybe it should: check in proper scoping context
mass <- 100
identical(mutate(starwars, mass = mass / !!mass),
          Mutate(starwars, mass = ~ mass / !!mass))

## This works in this example; to be reliable the implementation would
## need to use the formula environment.
mymass <- 100
identical(mutate(starwars, mass = mass / mymass),
          Mutate(starwars, mass = ~ mass / mymass))


##
## aggregate
##

## dplyr group summary
library(dplyr)
summarize(group_by(mtcars, gear, cyl), mpg = mean(mpg))

## aggregate group summary
aggregate(mpg ~ gear + cyl, mtcars, mean)

## list of data frames by group
v <- aggregate(. ~ gear + cyl, mtcars, list)
vv <- apply(v, 1, function(x) do.call(data.frame, as.list(x)))

# tildeverse: Embrace Formulas and Evauate the Standard Way

This is a simple and not (yet?) serious proof-of-concept for an
interface to tidyverse facilities based on using formulas and standard
evaluation.

A small subset f `dplyr` and the `aes` function of `ggplot2` are
handled for now. The names of the available functions are capitalized
forms of their tidyverse analogs, e.g. `Mutate`, `Group_by`, or `Aes`.

The cost of going this route is the need to write some `~` characters.
The benefit is that all evaluation iis standard, and functions
abstracting a commin pattern can be created without reorting to
complex quasiquotation operations.

An example posted by Lionel Henry using the new `{{` NSE operator:

```r
max_by <- function(data, var, by) {
    data %>%
        group_by({{ by }}) %>%
        summarize(maximum = max({{ var }}, na.rm = TRUE))
}
```

This would require either tools for splicint into a formula (whic
chreates issues with environments if there are multiple input
formulas) or a different approach. One possibility:

```r
Max_by <- function(data, var, by) {
    Transmute(data, var, by) %>%
        Group_by(by) %>%
        group_modify(function(x, ...) tibble(maximum = max(x[[1]])))
}
```

Easy to modify to allow multible `by` variables:

```r
Max_by <- function(data, var, ...) {
    Transmute(data, var, ...) %>%
        Group_by(...) %>%
        group_modify(function(x, ...) tibble(maximum = max(x[[1]])))
}
```

This is done by simple wrapper layer around the
standard tidyverse functions.

There are a number of

- It is quite likely that this is not entirely right in a number of
  ways as I don't claim to have mastered the tidyvers NSE framework,
  which seems to be needed to do this.

- A definitel flaw is that this approach ignores the environments of
  formulas. Handling tise properly for resolving references in
  formulas not found in the data frame would require deeper
  intervention into the implementations.

- Need to play around with
  - different variable capture possibilities
  - lexical capture
- Play around with parameterizing name and na.rm in max_by.

- reference: <https://tidyeval.tidyverse.org/>

- annoying comments about 'base R printer' in
    <https://rlang.r-lib.org/reference/quasiquotation.html> and
    `?rlang::quasiquotatio`.

## Very simple implementation of an interface without NSE.
## Ignores formula environments for now at least.

fixVar <- function(v) {
    if (class(v) == "formula") {
        if (length(v) != 2)
            stop("formula should have only a right hand side");
        v[[2]]
    }
    else v
}

Count <- function (x, ..., wt, sort, name, .drop) {
    if (! missing(wt)) stop("wt not supported yet")
    if (! missing(sort)) stop("sort not supported yet")
    if (! missing(name)) stop("name not supported yet")
    if (! missing(.drop)) stop(".drop not supported yet")

    dots <- lapply(list(...), fixVar)
    dplyr::count(x, !!!dots)
}

Group_by <- function (.data, ..., add = FALSE, .drop) {
    if (! missing(.drop)) stop(".drop not supported yet")

    dots <- lapply(list(...), fixVar)
    dplyr::group_by(.data, !!!dots, add = add)
}

Summarize <- function(.data, ...) {
    dots <- lapply(list(...), fixVar)
    dplyr::summarize(.data, !!!dots)
}

## Filter (not an ideal name ...)
Filter <- function(.data, ..., .preserve = FALSE) {
    dots <- lapply(list(...), fixVar)
    dplyr::filter(.data, !!!dots, .preserve = .preserve)
}

Mutate <- function(.data, ...) {
    dots <- lapply(list(...), fixVar)
    dplyr::mutate(.data, !!!dots)
}

Transmute <- function(.data, ...) {
    dots <- lapply(list(...), fixVar)
    dplyr::transmute(.data, !!!dots)
}

Arrange <- function(.data, ...) {
    dots <- lapply(list(...), fixVar)
    dplyr::arrange(.data, !!!dots)
}

## seem to need eval here
Aes <- function(...) {
    dots <- lapply(list(...), fixVar)
    eval(as.call(c(list(quote(ggplot2::aes)), dots)))
}

## can now define summarize_by withont NSE stuff
## https://twitter.com/_lionelhenry/status/1143447444891590656
Summarize_by <- function(data, ..., by)
    Summarize(Group_by(data, by), ...)
